<?php global $base_url; ?>
<!--<form action="<?php //echo $base_url . '/sample/insert'                             ?>" method="post" class="insert-form">-->
<form action="#" method="post" class="insert-form">
    <input type="text" name="name" placeholder="Name" required=""><br>
    <input type="email" name="email" placeholder="Email" required=""><br>
    <textarea name="message" placeholder="Message . ." required=""></textarea><br>
    <button type="submit">Send</button>
    <button type="reset">Clear</button>
</form>
<script type="text/javascript" src="<?php echo $GLOBALS['base_url'] . "/misc/jquery.js" ?>"></script>
<script type="text/javascript">
    var base_url = '<?php echo $base_url; ?>';
    $(document).ready(function () {
        var frm = $('.insert-form');
        frm.submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: base_url + '/sample/insert',
                data: frm.serializeArray(),
                success: function (msg) {
                    alert(msg);
                    frm.trigger("reset");
                }, error: function () {
                    alert('Could sent values');
                }
            });
        });
    });

</script>