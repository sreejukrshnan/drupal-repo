<button  class="btn">Get Value</button>
<div class="display"></div>

<script type="text/javascript" src="<?php echo $GLOBALS['base_url'] . "/misc/jquery.js" ?>"></script>
<script type="text/javascript">
<?php global $base_url; ?>
    var base_url = '<?php echo $base_url; ?>';

    $('.btn').click(function () {
        $.ajax({
            type: "GET",
            url: base_url + '/sample/ajax',
            success: function (msg) {
                //alert(msg);
                var obj = JSON.parse(msg);
                //console.log(obj);
                var table = "<table class='table table-responsive'><tr><th>Aid</th><th>Type</th><th>Callback</th><th>Parameter</th><th>Label</th></tr>";
                obj.forEach(function (e) {
                    table += "<tr>";
                    table += "<td>" + e.aid + "</td>";
                    table += "<td>" + e.type + "</td>";
                    table += "<td>" + e.callback + "</td>";
                    table += "<td>" + e.parameter + "</td>";
                    table += "<td>" + e.label + "</td>";
                    table += "</tr>";
                });
                table += "</table>";
                $('.display').html(table);
            },
            error: function () {
                alert('something went wrong!');
            }
        });
    });

</script>